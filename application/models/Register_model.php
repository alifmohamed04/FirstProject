<?php

class Register_model extends CI_Model
{

    // Function to select all from table name students.
    function show_listing()
    {
        $query = $this->db->get('register_form');
        $query_result = $query->result();
        return $query_result;
    }

    // Function to select particular record from table name students.
    function show_listings($data)
    {
        $this->db->select('*');
        $this->db->from('register_form');
        $this->db->where('First_Name', $data);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function delete_data($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('register_form');
    }

    function update_data($data, $id)
    {
        $this->db->where("id", $id);
        $this->db->update("form_register", $data);
        //UPDATE tbl_user SET first_name = '$first_name', last_name = '$last_name' WHERE id = '$id'
    }

}


?>
