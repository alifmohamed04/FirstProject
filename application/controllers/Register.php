<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    public function __construct()
    {
        parent::__construct();
    }

    function insert()
    {

        $First_Name = $this->input->post('First_Name');

        $Last_Name = $this->input->post('Last_Name');

        $Email = $this->input->post('Email');

        $Mobile_Phone = $this->input->post('Mobile_Phone');

        $Password = $this->input->post('Password');

        $Confirm_Password = $this->input->post('Confirm_Password');

        $this->load->view('form_register');


    }


    public function validation_form()
    {

        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        // required|regex_match[/^[0-9]{10}$/]

        $this->form_validation->
        set_rules(
            'First_Name',
            'First_Name',
            'trim|required|min_length[2]|max_length[12]');

        $this->form_validation->
        set_rules(
            'Last_Name',
            'Last_Name',
            'trim|required|min_length[2]|max_length[12]');

        $this->form_validation->
        set_rules(
            'Mobile_Phone',
            'Mobile_Phone',
            'trim|required|regex_match[[01]]|max_length[12]');

        $this->form_validation->
        set_rules(
            'Email',
            'Email',
            'trim|required|valid_email');

        $this->form_validation->
        set_rules(
            'Password',
            'Password',
            'trim|required|min_length[8]');

        $this->form_validation->
        set_rules(
            'Confirm_Password',
            'Confirm_Password',
            'trim|required|matches[Password]');


        if ($this->form_validation->run() == FALSE) {

            $this->load->view('form_register');
        } else {
            $post = $this->input->post();

            $data_array = array(
                'First_Name' => $post['First_Name'],
                'Last_Name' => $post['Last_Name'],
                'Email' => $post['Email'],
                'Mobile_Phone' => $post['Mobile_Phone'],
                'Password' => $post['Password'],
                'Confirm_Password' => $post['Confirm_Password']);


            $this->db->insert('register_form', $data_array);
            // var_dump ($post);
            // exit;


            redirect('register/listing');

        }


    }

    public function listing()
    {
        $data = array();
        $this->load->model('Register_model');
        $this->load->helper('url');

        $data['result'] = $this->Register_model->show_listing();
        //			 $this->updated_data();
        $this->load->view('listing_register', $data);


    }

    public function delete_data()
    {
        $data = array();
        $id = $this->uri->segment(3);
        $this->load->model("register_model");
        $this->register_model->delete_data($id);
        redirect('register/listing');

    }

    public function updated_data()
    {

        $post = $this->input->post();
        $First_Name = $post['First_Name'];
        $Last_Name = $post['Last_Name'];
        $Email = $post['Email'];
        $Mobile_Phone = $post['Mobile_Phone'];
        $Password = $post['Password'];
        $Confirm_Password = $post['Confirm_Password'];


        $data = array(
            'First_Name' => $First_Name,
            'Last_Name' => $Last_Name,
            'Email' => $Email,
            ' Mobile_Phone' => $Mobile_Phone,
            'Password' => $Password,
            'Confirm_Password' => $Confirm_Password
        );
        $id = $this->uri->segment(3);

        $this->db->where('id', $id);
        $this->load->model("register_model");
        $this->db->update('register_form', $data);
        redirect('register/listing');
    }


}
