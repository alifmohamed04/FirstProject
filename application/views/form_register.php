<html>
<head>
<body>


<title>Register Form </title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
      integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>


<form action="<?php echo site_url('register/validation_form'); ?>" method="post">
    <div class="container">

        <div class="row">


            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="First_Name">First Name</label>
                    <input type="text" class="form-control" name="First_Name"
                           value="<?php echo set_value('First_Name'); ?> "/>

                    <?php echo form_error('First_Name'); ?>

                </div>

                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="Last_Name">Last Name</label>
                        <input type="text" class="form-control" name="Last_Name"
                               value="<?php echo set_value('Last_Name'); ?>"/>
                        <?php echo form_error('Last_Name'); ?>


                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="Email">Email</label>
                            <input type="email" class="form-control" name="Email"
                                   value="<?php echo set_value('Email'); ?>"/>
                            <?php echo form_error('Email'); ?>


                        </div>


                        <div class="form-group col-md-6">
                            <label for="Mobile_Phone">Mobile Phone</label>
                            <input type="number" class="form-control" name="Mobile_Phone"
                                   value="<?php echo set_value('Mobile_Phone'); ?>"/>
                            <?php echo form_error('Mobile_Phone'); ?>

                        </div>


                        <div class="form-group col-md-6">
                            <label for="Password">Password</label>
                            <input type="password" class="form-control" name="Password"
                                   value="<?php echo set_value('Password'); ?>"/>
                            <?php echo form_error('Password'); ?>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="Confirm_Password">Confirm Password</label>
                            <input type="password" class="form-control" name="Confirm_Password"
                                   value="<?php echo set_value('Confirm_Password'); ?>"/>
                            <?php echo form_error('Confirm_Password'); ?>
                        </div>


                        <button type="submit" class="btn btn-primary">Sign up</button>

                    </div>
                </div>
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                        crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
                        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
                        crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
                        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
                        crossorigin="anonymous"></script>

</form>
</body>
</html>
<?php

if (isset($_GET['submit'])) {
    $Email = $_POST['Email'];

    foreach ($select->result() as $row) {
        $Email = $row->Email;

        if ($Email == $row) {
            delete($row);
        }
    }
}
?>
